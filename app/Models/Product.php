<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public function cart()
    {
        return $this->hasMany(Cart::class);
    }
    protected $fillable = [
        'project_name',
        'banner_img',
        'heading',
        'description',
        'price',
    ];
}
