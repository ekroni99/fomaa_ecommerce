<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable = [
        'name',
        'username',
        'password',
        'image',
        'status',
        'is_active'
    ];
    protected $primaryKey="id";
    protected $hidden = [
        'password',
        'remember_token',
    ];
}
