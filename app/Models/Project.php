<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    protected $fillable = [
        'project_name',
        'deadline',
        'working_status',
        'deliver_status',
        'Hosting_Charge',
        'Developer_charge',
        'Total_cost'
    ];
}
