<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }
    protected $fillable = [
        'name',
        'username',
        'password',
        'image',
        'status',
        'is_active'
    ];
    protected $primaryKey = "id";
    protected $hidden = [
        'password',
        'remember_token',
    ];
}
