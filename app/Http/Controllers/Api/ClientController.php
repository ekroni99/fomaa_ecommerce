<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $client = Client::all();
        echo count($client);
        if (count($client) > 0) {
            $response = [
                'message' => count($client) . "user is Found",
                'data' => $client,
                'status' => 1
            ];
        } else {
            $response = [
                'message' => count($client) . "user is Found",
                'status' => 0
            ];
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'username' => 'required|string|unique:clients|max:255',
            'password' => 'required|string|min:6',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif',
            'status' => 'nullable|boolean',
            'is_active' => 'nullable|boolean',
        ]);
        if ($validatedData->fails()) {
            return response()->json($validatedData->messages(), 400);
        } else {
            DB::beginTransaction();
            try {
                $client = new Client();
                $client->name = $request->name;
                $client->username = $request->username;
                $client->password = Hash::make($request->password);
                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    $filename = rand(1, 1000) . '_' . $client->name . '-' . $request->file('image')->getClientOriginalName();
                    $file->move('clients', $filename);
                    $client->image = $filename;
                }
                $client->save();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                p($e->getMessage());
                $client = null;
            }
            if ($client != null) {
                return response()->json([
                    'message' => 'client registered successfully'
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Internal server Error'
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $client = Client::find($id);

        if (is_null($client)) {
            return response()->json([
                'status' => 0,
                'message' => 'client not exits',
            ], 404);
        } else {
            DB::beginTransaction();
            try {
                $client->name = $request['name'];
                $client->username = $request['username'];
                if ($request->hasFile('newimage')) {
                    $destination = public_path('clients' . $request->oldimage);
                    if (file_exists($destination)) {
                        unlink($destination);
                    }
                    $file = $request->file('newimage');
                    $filename = rand(1, 1000) . '_' . $client->name . '-' . $request->file('newimage')->getClientOriginalName();
                    $file->move('clients', $filename);
                    $client->image = $filename;
                }
                $client->save();
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                $client = null;
            }
            if (is_null($client)) {
                return response()->json([
                    'status' => 0,
                    'message' => 'Internel server error',
                    'err_msg' => $th->getMessage()
                ], 500);
            } else {
                return response()->json([
                    'status' => 1,
                    'message' => 'client updated successfully'
                ], 200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $client = Client::find($id);
        if (is_null($client)) {
            $response = [
                'message' => "client not found",
                'status' => 0,
            ];
            $resCode = 404;
        } else {
            DB::beginTransaction();
            try {
                $client->delete();
                DB::commit();
                $response = [
                    'message' => 'client delete successfully',
                    'status' => 1
                ];
                $resCode = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response = [
                    'message' => 'Internal server error',
                    'status' => 0
                ];
                $resCode = 500;
            }
        }
        return response()->json($response, $resCode);
    }
}
