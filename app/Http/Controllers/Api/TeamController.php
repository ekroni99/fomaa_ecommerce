<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $team = Team::all();
        if (count($team) > 0) {
            $response = [
                'message' => count($team) . "user is Found",
                'data' => $team,
                'status' => 1
            ];
        } else {
            $response = [
                'message' => count($team) . "user is Found",
                'status' => 0
            ];
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'designation' => 'required|string|max:255',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'bio' => 'required|string',
            'details' => 'required|string',
            'status' => 'nullable|string',
        ]);
        if ($validatedData->fails()) {
            return response()->json($validatedData->messages(), 400);
        } else {
            DB::beginTransaction();
            try {
                $team = new Team();
                $team->name = $request->name;
                $team->designation = $request->designation;
                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    $filename = rand(1, 1000) . '_' . $team->name . '-' . $request->file('image')->getClientOriginalName();
                    $file->move('teams', $filename);
                    $team->image = $filename;
                }
                $team->bio = $request->bio;
                $team->details = $request->details;
                $team->save();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                p($e->getMessage());
                $team = null;
            }
            if ($team != null) {
                return response()->json([
                    'message' => 'team registered successfully'
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Internal server Error'
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $team = Team::find($id);
        // p($request->all());
        if (is_null($team)) {
            return response()->json([
                'status' => 0,
                'message' => 'team not exits',
            ], 404);
        } else {
            DB::beginTransaction();
            try {
                $team->name = $request['name'];
                $team->designation = $request['designation'];
                if ($request->hasFile('newimage')) {
                    $destination = public_path('teams' . $request->oldimage);
                    if (file_exists($destination)) {
                        unlink($destination);
                    }
                    $file = $request->file('newimage');
                    $filename = rand(1, 1000) . '_' . $team->name . '-' . $request->file('newimage')->getClientOriginalName();
                    $file->move('teams', $filename);
                    $team->image = $filename;
                }
                $team->bio = $request['bio'];
                $team->details = $request['details'];
                $team->save();
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                $team = null;
            }
            if (is_null($team)) {
                return response()->json([
                    'status' => 0,
                    'message' => 'Internel server error',
                    'err_msg' => $th->getMessage()
                ], 500);
            } else {
                return response()->json([
                    'status' => 1,
                    'message' => 'team updated successfully'
                ], 200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $team = Team::find($id);
        if (is_null($team)) {
            $response = [
                'message' => "team not found",
                'status' => 0,
            ];
            $resCode = 404;
        } else {
            DB::beginTransaction();
            try {
                $team->delete();
                DB::commit();
                $response = [
                    'message' => 'team delete successfully',
                    'status' => 1
                ];
                $resCode = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response = [
                    'message' => 'Internal server error',
                    'status' => 0
                ];
                $resCode = 500;
            }
        }
        return response()->json($response, $resCode);
    }
}
