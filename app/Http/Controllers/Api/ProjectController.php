<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $project = Project::all();
        if (count($project) > 0) {
            $response = [
                'message' => count($project) . "project is Found",
                'data' => $project,
                'status' => 1
            ];
        } else {
            $response = [
                'message' => count($project) . "project is not Found",
                'status' => 0
            ];
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'project_name'      => 'required|string|max:255',
            'client_id'         => 'required|exists:clients,id',
            'deadline'          => 'required|date',
            'category_id'       => 'required|exists:categories,id',
            'working_status'    => 'nullable|string|max:255',
            'deliver_status'    => 'nullable|string|max:255',
            'Hosting_Charge'    => 'required|numeric|min:0',
            'Developer_charge'  => 'required|numeric|min:0',
            'Total_cost'        => 'required|numeric|min:0',
        ]);
        if ($validatedData->fails()) {
            return response()->json($validatedData->messages(), 400);
        } else {
            DB::beginTransaction();
            try {
                $project = new Project();
                $project->project_name = $request->project_name;
                $project->project_id = $request->project_id;
                $project->deadline = $request->deadline;
                $project->category_id = $request->category_id;
                $project->Hosting_Charge = $request->Hosting_Charge;
                $project->Developer_charge = $request->Developer_charge;
                $project->Total_cost = $request->Total_cost;
                $project->save();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                p($e->getMessage());
                $project = null;
            }
            if ($project != null) {
                return response()->json([
                    'message' => 'project registered successfully'
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Internal server Error'
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $project = Project::find($id);
        if (is_null($project)) {
            return response()->json([
                'status' => 0,
                'message' => 'project not exits',
            ], 404);
        } else {
            DB::beginTransaction();
            try {
                $project->update([
                    'project_name' => $request->project_name,
                    'client_id' => $request->client_id,
                    'deadline' => $request->deadline,
                    'category_id' => $request->category_id,
                    'Hosting_Charge' => $request->Hosting_Charge,
                    'Developer_charge' => $request->Developer_charge,
                    'Total_cost' => $request->Total_cost,
                ]);
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                $project = null;
            }
            if (is_null($project)) {
                return response()->json([
                    'status' => 0,
                    'message' => 'Internel server error',
                    'err_msg' => $th->getMessage()
                ], 500);
            } else {
                return response()->json([
                    'status' => 1,
                    'message' => 'project updated successfully'
                ], 200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $project = Project::find($id);
        if (is_null($project)) {
            $response = [
                'message' => "project not found",
                'status' => 0,
            ];
            $resCode = 404;
        } else {
            DB::beginTransaction();
            try {
                $project->delete();
                DB::commit();
                $response = [
                    'message' => 'project delete successfully',
                    'status' => 1
                ];
                $resCode = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response = [
                    'message' => 'Internal server error',
                    'status' => 0
                ];
                $resCode = 500;
            }
        }
        return response()->json($response, $resCode);
    }
}
