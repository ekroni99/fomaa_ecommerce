<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $category = Category::all();
        echo count($category);
        if (count($category) > 0) {
            $response = [
                'message' => count($category) . "user is Found",
                'data' => $category,
                'status' => 1
            ];
        } else {
            $response = [
                'message' => count($category) . "user is Found",
                'status' => 0
            ];
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'category_name' => 'required|string|max:255',
            'status' => 'nullable|boolean',
        ]);
        if ($validatedData->fails()) {
            return response()->json($validatedData->messages(), 400);
        } else {
            DB::beginTransaction();
            try {
                $category = new Category();
                $category->category_name = $request->category_name;
                $category->save();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                p($e->getMessage());
                $category = null;
            }
            if ($category != null) {
                return response()->json([
                    'message' => 'category registered successfully'
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Internal server Error'
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $category = Category::find($id);

        if (is_null($category)) {
            return response()->json([
                'status' => 0,
                'message' => 'category not exits',
            ], 404);
        } else {
            DB::beginTransaction();
            try {
                $category->category_name = $request['category_name'];
                $category->save();
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                $category = null;
            }
            if (is_null($category)) {
                return response()->json([
                    'status' => 0,
                    'message' => 'Internel server error',
                    'err_msg' => $th->getMessage()
                ], 500);
            } else {
                return response()->json([
                    'status' => 1,
                    'message' => 'category updated successfully'
                ], 200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $category = Category::find($id);
        if (is_null($category)) {
            $response = [
                'message' => "category not found",
                'status' => 0,
            ];
            $resCode = 404;
        } else {
            DB::beginTransaction();
            try {
                $category->delete();
                DB::commit();
                $response = [
                    'message' => 'category delete successfully',
                    'status' => 1
                ];
                $resCode = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response = [
                    'message' => 'Internal server error',
                    'status' => 0
                ];
                $resCode = 500;
            }
        }
        return response()->json($response, $resCode);
    }
}
