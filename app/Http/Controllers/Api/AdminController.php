<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $admin = Admin::all();
        echo count($admin);
        if (count($admin) > 0) {
            $response = [
                'message' => count($admin) . "user is Found",
                'data' => $admin,
                'status' => 1
            ];
        } else {
            $response = [
                'message' => count($admin) . "user is Found",
                'status' => 0
            ];
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'username' => 'required|string|unique:developers|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validatedData->fails()) {
            return response()->json($validatedData->messages(), 400);
        } else {
            DB::beginTransaction();
            try {
                $admin = new Admin();
                $admin->name = $request->name;
                $admin->username = $request->username;
                $admin->password = Hash::make($request->password);
                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    $filename = rand(1, 1000) . '_' . $admin->name . '-' . $request->file('image')->getClientOriginalName();
                    $file->move('uploads', $filename);
                    $admin->image = $filename;
                }
                $admin->save();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                p($e->getMessage());
                $admin = null;
            }
            if ($admin != null) {
                return response()->json([
                    'message' => 'Admin registered successfully'
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Internal server Error'
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $admin = Admin::find($id);
        if (is_null($admin)) {
            $response = [
                'message' => "admin not found",
                'status' => 0
            ];
        } else {
            $response = [
                'message' => 'admin found',
                'status' => 1,
                'data' => $admin
            ];
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $admin = Admin::find($id);

        if (is_null($admin)) {
            return response()->json([
                'status' => 0,
                'message' => 'admin not exits',
            ], 404);
        } else {
            DB::beginTransaction();
            try {
                $admin->name = $request['name'];
                $admin->username = $request['username'];
                if ($request->hasFile('newimage')) {
                    $destination = public_path('uploads' . $request->oldimage);
                    if (file_exists($destination)) {
                        unlink($destination);
                    }
                    $file = $request->file('newimage');
                    $filename = rand(1, 1000) . '_' . $admin->name . '-' . $request->file('newimage')->getClientOriginalName();
                    $file->move('uploads', $filename);
                    $admin->image = $filename;
                }
                $admin->save();
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                $admin = null;
            }
            if (is_null($admin)) {
                return response()->json([
                    'status' => 0,
                    'message' => 'Internel server error',
                    'err_msg' => $th->getMessage()
                ], 500);
            } else {
                return response()->json([
                    'status' => 1,
                    'message' => 'admin updated successfully'
                ], 200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $admin = Admin::find($id);
        if (is_null($admin)) {
            $response = [
                'message' => "admin not found",
                'status' => 0,
            ];
            $resCode = 404;
        } else {
            DB::beginTransaction();
            try {
                $admin->delete();
                DB::commit();
                $response = [
                    'message' => 'admin delete successfully',
                    'status' => 1
                ];
                $resCode = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response = [
                    'message' => 'Internal server error',
                    'status' => 0
                ];
                $resCode = 500;
            }
        }
        return response()->json($response, $resCode);
    }
}
