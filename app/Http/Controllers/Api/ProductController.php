<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product = Product::all();
        echo count($product);
        if (count($product) > 0) {
            $response = [
                'message' => count($product) . " product is Found",
                'data' => $product,
                'status' => 1
            ];
        } else {
            $response = [
                'message' => count($product) . " product is Found",
                'status' => 0
            ];
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'project_name' => 'required|string|max:255',
            'banner_img'   => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'heading'      => 'required|string|max:255',
            'description'  => 'required|string',
            'price'        => 'required|numeric|min:0',
        ]);
        if ($validatedData->fails()) {
            return response()->json($validatedData->messages(), 400);
        } else {
            DB::beginTransaction();
            try {
                $product = new Product();
                $product->project_name = $request->project_name;
                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    $filename = rand(1, 1000) . '_' . $product->name . '-' . $request->file('image')->getClientOriginalName();
                    $file->move('products', $filename);
                    $product->banner_img = $filename;
                }
                $product->heading = $request->heading;
                $product->description = $request->description;
                $product->price = $request->price;
                $product->save();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                p($e->getMessage());
                $product = null;
            }
            if ($product != null) {
                return response()->json([
                    'message' => 'product registered successfully'
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Internal server Error'
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $product = Product::find($id);

        if (is_null($product)) {
            return response()->json([
                'status' => 0,
                'message' => 'product not exits',
            ], 404);
        } else {
            DB::beginTransaction();
            try {
                $product->project_name = $request->project_name;
                if ($request->hasFile('newimage')) {
                    $destination = public_path('products' . $request->oldimage);
                    if (file_exists($destination)) {
                        unlink($destination);
                    }
                    $file = $request->file('newimage');
                    $filename = rand(1, 1000) . '_' . $product->name . '-' . $request->file('newimage')->getClientOriginalName();
                    $file->move('products', $filename);
                    $product->banner_img = $filename;
                }
                $product->heading = $request->heading;
                $product->description = $request->description;
                $product->price = $request->price;
                $product->save();
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                $product = null;
            }
            if (is_null($product)) {
                return response()->json([
                    'status' => 0,
                    'message' => 'Internel server error',
                    'err_msg' => $th->getMessage()
                ], 500);
            } else {
                return response()->json([
                    'status' => 1,
                    'message' => 'product updated successfully'
                ], 200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product = Product::find($id);
        if (is_null($product)) {
            $response = [
                'message' => "product not found",
                'status' => 0,
            ];
            $resCode = 404;
        } else {
            DB::beginTransaction();
            try {
                $product->delete();
                DB::commit();
                $response = [
                    'message' => 'product delete successfully',
                    'status' => 1
                ];
                $resCode = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response = [
                    'message' => 'Internal server error',
                    'status' => 0
                ];
                $resCode = 500;
            }
        }
        return response()->json($response, $resCode);
    }
}
