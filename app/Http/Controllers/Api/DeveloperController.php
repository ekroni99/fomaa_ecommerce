<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Developer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class DeveloperController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $developer = Developer::all();
        if (count($developer) > 0) {
            $response = [
                'message' => count($developer) . "user is Found",
                'data' => $developer,
                'status' => 1
            ];
        } else {
            $response = [
                'message' => count($developer) . "user is Found",
                'status' => 0
            ];
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'username' => 'required|string|unique:developers|max:255',
            'password' => 'required|string|min:6',
            'designation' => 'required|string|max:255',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'skills.*' => 'nullable|string',
            'status' => 'nullable|boolean',
            'is_active' => 'nullable|boolean',
        ]);
        if ($validatedData->fails()) {
            return response()->json($validatedData->messages(), 400);
        } else {
            DB::beginTransaction();
            try {
                $developer = new Developer();
                $developer->name = $request->name;
                $developer->username = $request->username;
                $developer->password = Hash::make($request->password);
                $developer->designation = $request->designation;
                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    $filename = rand(1, 1000) . '_' . $developer->name . '-' . $request->file('image')->getClientOriginalName();
                    $file->move('developers', $filename);
                    $developer->image = $filename;
                }
                $developer->skills = $request->skills;
                $developer->save();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                p($e->getMessage());
                $developer = null;
            }
            if ($developer != null) {
                return response()->json([
                    'message' => 'developer registered successfully'
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Internal server Error'
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $developer = Developer::find($id);
        if (is_null($developer)) {
            $response = [
                'message' => "developer not found",
                'status' => 0
            ];
        } else {
            $response = [
                'message' => 'developer found',
                'status' => 1,
                'data' => $developer
            ];
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $developer = Developer::find($id);

        if (is_null($developer)) {
            return response()->json([
                'status' => 0,
                'message' => 'developer not exits',
            ], 404);
        } else {
            DB::beginTransaction();
            try {
                $developer->name = $request['name'];
                $developer->username = $request['username'];
                if ($request->hasFile('newimage')) {
                    $destination = public_path('developer' . $request->oldimage);
                    if (file_exists($destination)) {
                        unlink($destination);
                    }
                    $file = $request->file('newimage');
                    $filename = rand(1, 1000) . '_' . $developer->name . '-' . $request->file('newimage')->getClientOriginalName();
                    $file->move('developers', $filename);
                    $developer->image = $filename;
                }
                $developer->skills = $request->skills;
                $developer->save();
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                $developer = null;
            }
            if (is_null($developer)) {
                return response()->json([
                    'status' => 0,
                    'message' => 'Internel server error',
                    'err_msg' => $th->getMessage()
                ], 500);
            } else {
                return response()->json([
                    'status' => 1,
                    'message' => 'developer updated successfully'
                ], 200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $developer = Developer::find($id);
        if (is_null($developer)) {
            $response = [
                'message' => "developer not found",
                'status' => 0,
            ];
            $resCode = 404;
        } else {
            DB::beginTransaction();
            try {
                $developer->delete();
                DB::commit();
                $response = [
                    'message' => 'developer delete successfully',
                    'status' => 1
                ];
                $resCode = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response = [
                    'message' => 'Internal server error',
                    'status' => 0
                ];
                $resCode = 500;
            }
        }
        return response()->json($response, $resCode);
    }
}
