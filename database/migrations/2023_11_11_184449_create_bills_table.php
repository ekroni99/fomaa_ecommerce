<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->json('products_id');
            $table->json('quantity');
            $table->json('cost');
            $table->decimal('total_cost', 10, 2);
            $table->boolean('bill_generate_status')->comment("1:Active,0:Inactive")->default(1);
            $table->boolean('delivery_status')->comment("1:Active,0:Inactive")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bills');
    }
};
