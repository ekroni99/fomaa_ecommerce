<?php

use App\Http\Controllers\Api\AdminController;
use App\Http\Controllers\Api\BillController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\clientController;
use App\Http\Controllers\Api\DeveloperController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\TeamController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


//for admin
Route::post('admin/store',[AdminController::class, 'store']);
Route::get('admin/get',[AdminController::class, 'index']);
Route::get('admin/show/{id}',[AdminController::class, 'show']);
Route::delete('admin/delete/{id}',[AdminController::class, 'destroy']);
Route::put('admin/update/{id}',[AdminController::class, 'update']);

//for developer
Route::post('developer/store',[DeveloperController::class, 'store']);
Route::get('developer/get',[DeveloperController::class, 'index']);
Route::get('developer/show/{id}',[DeveloperController::class, 'show']);
Route::delete('developer/delete/{id}',[DeveloperController::class, 'destroy']);
Route::put('developer/update/{id}',[DeveloperController::class, 'update']);

//for client
Route::post('client/store',[ClientController::class,'store']);
Route::get('client/get',[ClientController::class,'index']);
Route::get('client/show/{id}',[ClientController::class,'show']);
Route::delete('client/delete/{id}',[ClientController::class,'destroy']);
Route::put('client/update/{id}',[ClientController::class,'update']);

//for category
Route::post('category/store',[CategoryController::class,'store']);
Route::get('category/get',[CategoryController::class,'index']);
Route::get('category/show/{id}',[CategoryController::class,'show']);
Route::delete('category/delete/{id}',[CategoryController::class,'destroy']);
Route::put('category/update/{id}',[CategoryController::class,'update']);

//for projects
Route::post('project/store',[ProjectController::class,'store']);
Route::get('project/get',[ProjectController::class,'index']);
Route::get('project/show/{id}',[ProjectController::class,'show']);
Route::delete('project/delete/{id}',[ProjectController::class,'destroy']);
Route::put('project/update/{id}',[ProjectController::class,'update']);

//for product
Route::post('product/store',[ProductController::class,'store']);
Route::get('product/get',[ProductController::class,'index']);
Route::get('product/show/{id}',[ProductController::class,'show']);
Route::delete('product/delete/{id}',[ProductController::class,'destroy']);
Route::put('product/update/{id}',[ProductController::class,'update']);

//for team
Route::post('team/store',[TeamController::class,'store']);
Route::get('team/get',[TeamController::class,'index']);
Route::get('team/show/{id}',[TeamController::class,'show']);
Route::delete('team/delete/{id}',[TeamController::class,'destroy']);
Route::put('team/update/{id}',[TeamController::class,'update']);

//for cart
Route::post('cart/store',[CartController::class,'store']);
Route::get('cart/get',[CartController::class,'index']);
Route::get('cart/show/{id}',[CartController::class,'show']);
Route::delete('cart/delete/{id}',[CartController::class,'destroy']);
Route::put('cart/update/{id}',[CartController::class,'update']);

//for bill
Route::post('bill/store',[BillController::class,'store']);
Route::get('bill/get',[BillController::class,'index']);
Route::get('bill/show/{id}',[BillController::class,'show']);
Route::delete('bill/delete/{id}',[BillController::class,'destroy']);
Route::put('bill/update/{id}',[BillController::class,'update']);